#+title: Modules : « Système Linux »
#+author: Olivier Le Monnier
#+email: olm@unicaen.fr
#+language: fr
#+select_tags: export
#+exclude_tags: noexport
#+html_link_home: http://lemonniero.pages.unicaen.fr/
#+html_link_up: http://lemonniero.pages.unicaen.fr/

#+index: Home

  - [[./m22-m24.org][M22/M24 : Remise à niveau « Système »]], /20h/13h/, fin
    septembre-début octobre
  - [[./m61][M61, Sécurité du réseau : Les menaces]], /4h/, le 14 novembre
  - [[./m62.org][M62, Sécurité Linux avancée]], SELinux, /10h/, mi-janvier
  - [[./m81.org][M81, Intrusion : définitions, détection, prévention]], /16h/, avril-mai
