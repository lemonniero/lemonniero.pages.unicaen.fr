#+title: M81 : Détection d'intrusion
#+language: fr
#+select_tags: export
#+exclude_tags: noexport
#+html_link_home: http://lemonniero.pages.unicaen.fr/
#+html_link_up: http://lemonniero.pages.unicaen.fr/m81.html
#+options: toc:nil num:nil

#+index: Securité
#+index: IDS

Préambule : [[./centos8-over.org][CentOS n'est plus maintenue !]]

* Tester Endlessh

  https://github.com/skeeto/endlessh/blob/master/README.md

